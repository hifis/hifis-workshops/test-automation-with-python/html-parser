<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)
SPDX-License-Identifier: MIT
-->

# We Welcome your Contribution!

## General Options

- Please open **an issue**, if you have improvement ideas.
- Please open **a merge request**, if you want to share your improvements.

## Providing a Merge Request

- Please note that this **is not** a usual Git repository:
  - It demonstrates every important step performed in the workshop.
  - Every branch provides the basis for different exercises in the workshop.
- If you provide a merge request:
  - Create the merge request against the branch which initially introduces the content that you want to change.
  - Then, we will take over and rebase all branches building on it.
